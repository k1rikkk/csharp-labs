﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2.Task3
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Console.Write("Enter number of rows: ");
            var rows = 0;
            if (!int.TryParse(Console.ReadLine(), out rows))
            {
                Console.WriteLine("Number of rows must be an integer number");
                return;
            }
            Console.Write("Enter number of cols: ");
            var cols = 0;
            if (!int.TryParse(Console.ReadLine(), out cols))
            {
                Console.WriteLine("Number of columns must be an integer number");
                return;
            }

            var array = new float[rows, cols];
            var ans = -1;
            for (var j = 0; j < cols; j++)
            {
                bool hasZero = false;
                for (var i = 0; i < rows; i++)
                {
                    Console.Write($"a[{i}, {j}] = ");
                    if (!float.TryParse(Console.ReadLine(), out array[i, j]))
                    {
                        Console.WriteLine("Elements must be a float number");
                        return;
                    }
                    if (array[i, j] == 0)
                        hasZero = true;
                }
                if (hasZero)
                    ans = j;
            }
            if (ans == -1)
                Console.WriteLine("Array doesn\'t contains zeros");
            else
                Console.WriteLine($"Last column containing zeros is {ans + 1}");
            Console.Read();
        }
    }
}
