﻿using System;

namespace Lab1.Task1
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Console.Write("Enter x: ");
            var x = 0f;
            try
            {
                x = float.Parse(Console.ReadLine());
            }
            catch (FormatException)
            {
                Console.WriteLine("X must be float number");
                Console.Read();
                return;
            }
            var y = 0f;
            if (x < -3)
                y = x + 3;
            else if (x < 0)
                y = (float)Math.Sqrt(9 - x * x);
            else if (x < 6)
                y = -x / 2 + 3;
            else
                y = x - 6;
            Console.WriteLine($"f({x}) = {y}");
            Console.Read();
        }
    }
}
