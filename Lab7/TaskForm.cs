﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab7
{
    public partial class TaskForm : Form
    {
        public TaskForm()
        {
            InitializeComponent();
        }

        private void ButtonOk_Click(object sender, EventArgs e)
        {
            var text = textBoxInput.Text;

            if (checkBoxUC.Checked)
                text = text.ToUpper();
            if (checkBoxReverse.Checked)
                text = new string(text.Reverse().ToArray());

            textBoxInput.Text = text;
        }
    }
}
