﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab7
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void ButtonExit_Click(object sender, EventArgs e) => Close();

        private void ButtonAbout_Click(object sender, EventArgs e) => MessageBox.Show("Ковалёв К.А. ПО-31 Вариант 13");

        private void ButtonReverse_Click(object sender, EventArgs e) => new TaskForm().Show();
    }
}
