﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace Lab8
{
    public partial class MainForm : Form
    {
        private Func<double, double>[] funcs = new Func<double, double>[] { x => 1 - x, x => 3 / (x + 3), x => Math.Exp(x), x => 5 };

        private string funcName;
        private Func<double, double> func = null;

        private double start = -5;
        private double end = 5;
        private double step = 0.01f;

        public MainForm()
        {
            InitializeComponent();
        }

        private void QuitToolStripMenuItem_Click(object sender, EventArgs e) => Close();

        private void AboutToolStripMenuItem_Click(object sender, EventArgs e) => MessageBox.Show(Text);

        private void ClearToolStripMenuItem_Click(object sender, EventArgs e) => chart.Series.Clear();

        private void BuildToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (func == null)
            {
                MessageBox.Show("Выберите график");
                return;
            }

            chart.Series.Clear();
            chart.ChartAreas.Clear();
            chart.ChartAreas.Add("ChartArea");

            var series = new Series(funcName)
            {
                XValueType = ChartValueType.Double,
                YValueType = ChartValueType.Double,
                ChartType = SeriesChartType.Line
            };
            if (funcName == "y = 3 / (x + 3)")
            {
                var area = chart.ChartAreas[0];
                area.AxisY.Maximum = 5;
                area.AxisY.Minimum = -5;
            }
            for (var x = start; x <= end; x += step)
                series.Points.AddXY(x, func(x));

            chart.Series.Add(series);
        }

        private void ChartToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var form = new ChartsForm();
            if (form.ShowDialog() != DialogResult.OK || form.Selected < 0)
                return;
            func = funcs[form.Selected];
            funcName = form.SelectedName;
        }
    }
}
