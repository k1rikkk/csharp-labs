﻿
using System.Windows.Forms;

namespace Lab8
{
    public partial class ChartsForm : Form
    {
        public ChartsForm()
        {
            InitializeComponent();
        }

        public int Selected => listBox.SelectedIndex;
        public string SelectedName => listBox.SelectedIndex < 0 ? null : listBox.Items[listBox.SelectedIndex].ToString();
    }
}
