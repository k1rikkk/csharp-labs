﻿using System;
using System.Linq;

namespace Lab6
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Console.WriteLine("Enter float elements of array, separate by commas:");
            var list = new float[] { };
            try
            {
                list = Console.ReadLine().Split(new[] { " , ", ", ", " ,", "," }, StringSplitOptions.RemoveEmptyEntries).Select(x => float.Parse(x)).ToArray();
            }
            catch (FormatException)
            {
                Console.WriteLine("Can\'t parse array. Enter float elements of array, separate by commas: ");
                return;
            }

            list = list.OrderBy(x => x < 0 ? 1 : 0).ToArray();

            Console.WriteLine("Result:");
            Console.WriteLine(string.Join(", ", list));
            Console.Read();
        }
    }
}
