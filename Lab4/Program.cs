﻿using System;

namespace Lab4
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var a = new Vector(1, 1, 1);
            var b = new Vector(3, 1, 6);
            var c = new Vector(-2, 3, -1);

            Console.WriteLine(a.Length());
            Console.WriteLine(a + b);
            Console.WriteLine(a - c);
            Console.WriteLine(b * c);
            Console.WriteLine(Vector.AngleCos(b, c));
            Console.Read();
        }
    }
}
