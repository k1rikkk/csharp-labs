﻿using System;

namespace Lab4
{
    public class Vector
    {
        public float X { get; set; }
        public float Y { get; set; }
        public float Z { get; set; }

        public Vector() { }

        public Vector(float x, float y)
        {
            X = x;
            Y = y;
        }

        public Vector(float x, float y, float z) : this(x, y)
        {
            Z = z;
        }

        public static Vector operator +(Vector a, Vector b) => new Vector { X = a.X + b.X, Y = a.Y + b.Y, Z = a.Z + b.Z };
        public static Vector operator -(Vector a) => new Vector { X = -a.X, Y = -a.Y, Z = -a.Z };
        public static Vector operator -(Vector a, Vector b) => a + (-b);

        public static float operator *(Vector a, Vector b) => a.X * b.X + a.Y * b.Y + a.Z * b.Z;

        public float Length() => (float)Math.Sqrt(X * X + Y * Y + Z * Z);

        public static float AngleCos(Vector a, Vector b) => a * b / a.Length() / b.Length();

        public override string ToString() => $"({X}, {Y}, {Z})";
    }
}
