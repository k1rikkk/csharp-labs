﻿using System;
using System.Linq;

namespace Lab2.Task1
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Console.WriteLine("Enter float elements of array, separate by commas:");
            var list = new float[] { };
            try
            {
                list = Console.ReadLine().Split(new[] { " , ", ", ", " ,", "," }, StringSplitOptions.RemoveEmptyEntries).Select(x => float.Parse(x)).ToArray();
            }
            catch (FormatException)
            {
                Console.WriteLine("Can\'t parse array. Enter float elements of array, separate by commas: ");
                return;
            }
            var mul = 1f;
            foreach (var x in list)
                if (x < 0)
                    mul *= x;
            Console.WriteLine($"Multiplication of negative elements: {mul}");
            Console.Read();
        }
    }
}
