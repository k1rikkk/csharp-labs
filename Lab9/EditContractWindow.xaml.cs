﻿using Lab9.Data;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Lab9
{
    /// <summary>
    /// Interaction logic for EditContractWindow.xaml
    /// </summary>
    public partial class EditContractWindow : Window
    {
        private readonly IStorage storage;
        private readonly Guid? contractId;
        private readonly Action onSave;

        public EditContractWindow(IStorage storage, Action onSave = null, Guid? id = null)
        {
            this.storage = storage;
            contractId = id;
            this.onSave = onSave;

            InitializeComponent();

            var tours = storage.Tours.OrderBy(t => t.Title).ToList();
            tourId.Items.Clear();
            tourId.Items.Add("Выберите тур");
            foreach (var tour in tours)
                tourId.Items.Add(tour);
            tourId.SelectedIndex = 0;

            if (id != null)
            {
                var contract = storage.Contracts.Single(c => c.Id == id);
                client.Text = contract.ClientName;
                employee.Text = contract.EmployeeName;
                phone.Text = contract.ClientPhone;
                cost.Text = contract.Cost.ToString();
                date.SelectedDate = contract.DateTime;
                number.Text = contract.Number;
                places.Text = contract.Places.ToString();
                placesSlider.Value = contract.Places;
                tourId.SelectedIndex = tours.FindIndex(t => t.Id == contract.TourId) + 1;
            }
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            var contract = GetModel();
            if (contractId == null)
                storage.Add(contract);
            else
                storage.Update(contract);
            storage.SaveChanges();
            Close();
            onSave?.Invoke();
        }

        private Contract GetModel()
        {
            Contract contract = null;
            if (contractId == null)
            {
                contract = new Contract
                {
                    Id = Guid.NewGuid(),
                    ClientName = client.Text,
                    EmployeeName = employee.Text,
                    ClientPhone = phone.Text,
                    Cost = decimal.Parse(cost.Text),
                    DateTime = date.SelectedDate.Value,
                    Number = number.Text,
                    Places = int.Parse(places.Text),
                    TourId = ((Tour)tourId.SelectedItem).Id
                };
                storage.Add(contract);
            }
            else
            {
                contract = storage.Contracts.Single(t => t.Id == contractId);
                contract.Id = Guid.NewGuid();
                contract.ClientName = client.Text;
                contract.EmployeeName = employee.Text;
                contract.ClientPhone = phone.Text;
                contract.Cost = decimal.Parse(cost.Text);
                contract.DateTime = date.SelectedDate.Value;
                contract.Number = number.Text;
                contract.Places = int.Parse(places.Text);
                contract.TourId = ((Tour)tourId.SelectedItem).Id;
                storage.Update(contract);
            }
            return contract;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e) => Close();

        private void PlacesSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e) => places.Text = ((int)placesSlider.Value).ToString();

        private void SaveFileButton_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new SaveFileDialog();
            if (dialog.ShowDialog() == true)
            {
                var model = GetModel();
                model.Tour = null;
                FileStorage.Save(model, dialog.FileName);
            }
        }

        private void LoadFileButton_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new OpenFileDialog();
            if (dialog.ShowDialog() == true)
            {
                var contract = FileStorage.Load<Contract>(dialog.FileName);
                client.Text = contract.ClientName;
                employee.Text = contract.EmployeeName;
                phone.Text = contract.ClientPhone;
                cost.Text = contract.Cost.ToString();
                date.SelectedDate = contract.DateTime;
                number.Text = contract.Number;
                places.Text = contract.Places.ToString();
                placesSlider.Value = contract.Places;
                               
                var tours = storage.Tours.OrderBy(t => t.Title).ToList();
                tourId.Items.Clear();
                tourId.Items.Add("Выберите тур");
                foreach (var tour in tours)
                    tourId.Items.Add(tour);
                tourId.SelectedIndex = tours.FindIndex(t => t.Id == contract.TourId) + 1;
            }
        }
    }
}
