﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Lab9.Data
{
    public class Queries
    {
        public IStorage Storage { get; set; }

        public IEnumerable<Contract> ContractsByDate(DateTime date) => Storage.Contracts.Where(c => c.DateTime == date);

        public IEnumerable<dynamic> EmployeesContracts() => Storage.Contracts.GroupBy(c => c.EmployeeName).Select(kv => new { Employee = kv.Key, Count = kv.Count() });

        public IEnumerable<Tour> ToursMinDuration(int days) => Storage.Tours.Where(t => t.Days > days);

        public IEnumerable<Tour> ToursByClient(string client) => Storage.Contracts.Where(c => c.ClientName.Contains(client)).Select(c => c.Tour);
    }
}
