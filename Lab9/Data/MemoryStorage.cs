﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Lab9.Data
{
    public class MemoryStorage : IStorage
    {
        protected List<Tour> tours = new List<Tour>
        {
            new Tour
            {
                Id = Guid.Parse("1fe4de17-3bb7-4da4-a021-95017221b628"),
                Title = "Намаальный тур",
                Begin = DateTime.Parse("02.02.2019"),
                End = DateTime.Parse("25.02.2019"),
                Days = 21,
                HotelLevel = "5 звезд",
                RestPlace = "Ницца, Франция"
            },
            new Tour
            {
                Id = Guid.Parse("b69e4563-921f-4c7e-aab0-6beac1e94224"),
                Title = "Ниче такой тур",
                Begin = DateTime.Parse("12.05.2019"),
                End = DateTime.Parse("30.05.2019"),
                Days = 14,
                HotelLevel = "4 звезды",
                RestPlace = "Рим, Италия"
            }
        };
        protected List<Contract> contracts = new List<Contract>
        {
            new Contract
            {
                Id = Guid.Parse("3156672b-e98e-414c-9ae6-216a397cf924"),
                Number = "7181928950",
                TourId = Guid.Parse("1fe4de17-3bb7-4da4-a021-95017221b628"),
                ClientName = "Данькин Степан",
                EmployeeName = "Редругина Алиса",
                ClientPhone = "375 (44) 742-10-06",
                Cost = 3199.99m,
                DateTime = DateTime.Parse("23.11.2018"),
                Places = 4
            },
            new Contract
            {
                Id = Guid.Parse("94fac8cd-fe8a-4022-a83a-d618efb1e29a"),
                Number = "7653883570",
                TourId = Guid.Parse("b69e4563-921f-4c7e-aab0-6beac1e94224"),
                ClientName = "Дюженков Алексей",
                EmployeeName = "Николаева Полина",
                ClientPhone = "375 (29) 467-38-11",
                Cost = 1899.99m,
                DateTime = DateTime.Parse("21.04.2019"),
                Places = 2
            }
        };

        public IEnumerable<Tour> Tours => tours.Select(t =>
        {
            t.Contracts = contracts.Where(c => c.TourId == t.Id).ToList();
            return t;
        });

        public IEnumerable<Contract> Contracts => contracts.Select(c =>
        {
            c.Tour = tours.Single(t => t.Id == c.TourId);
            return c;
        });

        public void Add<TModel>(TModel model) where TModel : Model => ((List<TModel>)Set<TModel>()).Add(model);

        public void Remove<TModel>(TModel model) where TModel : Model
        {
            var set = (List<TModel>)Set<TModel>();
            var toRemove = set.Single(m => m.Id == model.Id);
            set.Remove(toRemove);
        }

        public void Update<TModel>(TModel model) where TModel : Model
        {
            var set = (List<TModel>)Set<TModel>();
            var index = set.FindIndex(m => m.Id == model.Id);
            set[index] = model;
        }

        public int SaveChanges() => 0;

        protected IEnumerable<Model> Set<TModel>() where TModel: Model
        {
            var type = typeof(TModel);
            if (type == typeof(Tour))
                return tours;
            else if (type == typeof(Contract))
                return contracts;
            throw new System.Exception("Unsupported set type");
        }

        public void Dispose() { }
    }
}
