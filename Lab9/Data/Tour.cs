﻿using System;
using System.Collections.Generic;

namespace Lab9.Data
{
    public class Tour : Model
    {
        public string Title { get; set; }
        public int Days { get; set; }
        public string RestPlace { get; set; }
        public DateTime Begin { get; set; }
        public DateTime End { get; set; }
        public string HotelLevel { get; set; }

        public virtual List<Contract> Contracts { get; set; }

        public override string ToString() => Title + " " + Begin.ToString("dd.MM.yyyy") + " " + End.ToString("dd.MM.yyyy");
    }
}
