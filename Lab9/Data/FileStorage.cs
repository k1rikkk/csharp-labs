﻿using Newtonsoft.Json;

namespace Lab9.Data
{
    public class FileStorage
    {
        public static void Save<TModel>(TModel model, string fileName) => System.IO.File.WriteAllText(fileName, JsonConvert.SerializeObject(model));

        public static TModel Load<TModel>(string fileName) => JsonConvert.DeserializeObject<TModel>(System.IO.File.ReadAllText(fileName));
    }
}
