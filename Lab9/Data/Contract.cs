﻿using System;

namespace Lab9.Data
{
    public class Contract : Model
    {
        public string Number { get; set; }
        public Guid TourId { get; set; }
        public DateTime DateTime { get; set; }
        public string EmployeeName { get; set; }
        public string ClientName { get; set; }
        public string ClientPhone { get; set; }
        public decimal Cost { get; set; }
        public int Places { get; set; }

        public virtual Tour Tour { get; set; }
    }
}
