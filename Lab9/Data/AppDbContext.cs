﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;

namespace Lab9.Data
{
    public class AppDbContext : DbContext, IStorage
    {
        public DbSet<Tour> Tours { get; set; }
        public DbSet<Contract> Contracts { get; set; }


        public AppDbContext() : base("Server=DESKTOP-KNQ5R8C\\SQLSERVER;Database=SPPLab9;Trusted_Connection=True;") { }

        IEnumerable<Contract> IStorage.Contracts => Contracts.Include(c => c.Tour);
        IEnumerable<Tour> IStorage.Tours => Tours.Include(t => t.Contracts);
        public void Add<TModel>(TModel model) where TModel: Model => Set<TModel>().Add(model);
        public void Update<TModel>(TModel model) where TModel: Model { }
        public void Remove<TModel>(TModel model) where TModel: Model => Set<TModel>().Remove(model);
    }
}
