﻿using System;
using System.Collections.Generic;

namespace Lab9.Data
{
    public interface IStorage : IDisposable
    {
        IEnumerable<Contract> Contracts { get; }
        IEnumerable<Tour> Tours { get; }
        void Add<TModel>(TModel model) where TModel: Model;
        void Update<TModel>(TModel model) where TModel : Model;
        void Remove<TModel>(TModel model) where TModel : Model;
        int SaveChanges();
    }
}
