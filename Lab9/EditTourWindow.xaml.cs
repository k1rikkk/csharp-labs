﻿using Lab9.Data;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Lab9
{
    /// <summary>
    /// Interaction logic for EditTourWindow.xaml
    /// </summary>
    public partial class EditTourWindow : Window
    {
        private readonly IStorage storage;
        private readonly Guid? tourId;
        private readonly Action onSave;

        public EditTourWindow(IStorage storage, Action onSave = null, Guid? id = null)
        {
            this.storage = storage;
            tourId = id;
            this.onSave = onSave;

            InitializeComponent();

            if (id != null)
            {
                var tour = storage.Tours.Single(t => t.Id == id);
                title.Text = tour.Title;
                days.Text = tour.Days.ToString();
                daysSlider.Value = tour.Days;
                begin.SelectedDate = tour.Begin;
                end.SelectedDate = tour.End;
                restPlace.Text = tour.RestPlace;
                hotelLevel.Text = tour.HotelLevel;
            }
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            var tour = GetModel();
            if (tourId == null)
                storage.Add(tour);
            else
                storage.Update(tour);
            storage.SaveChanges();
            Close();
            onSave?.Invoke();
        }

        private Tour GetModel()
        {
            Tour tour = null;
            if (tourId == null)
            {
                tour = new Tour
                {
                    Id = Guid.NewGuid(),
                    Title = title.Text,
                    Days = int.Parse(days.Text),
                    Begin = begin.SelectedDate.Value,
                    End = end.SelectedDate.Value,
                    RestPlace = restPlace.Text,
                    HotelLevel = hotelLevel.Text
                };
                storage.Add(tour);
            }
            else
            {
                tour = storage.Tours.Single(t => t.Id == tourId);
                tour.Title = title.Text;
                tour.Days = int.Parse(days.Text);
                tour.Begin = begin.SelectedDate.Value;
                tour.End = end.SelectedDate.Value;
                tour.RestPlace = restPlace.Text;
                tour.HotelLevel = hotelLevel.Text;
                storage.Update(tour);
            }
            return tour;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e) => Close();

        private void DaysSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e) => days.Text = ((int)daysSlider.Value).ToString();

        private void LoadFileButton_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new OpenFileDialog();
            if (dialog.ShowDialog() == true)
            {
                var tour = FileStorage.Load<Tour>(dialog.FileName);
                title.Text = tour.Title;
                days.Text = tour.Days.ToString();
                daysSlider.Value = tour.Days;
                begin.SelectedDate = tour.Begin;
                end.SelectedDate = tour.End;
                restPlace.Text = tour.RestPlace;
                hotelLevel.Text = tour.HotelLevel;
            }
        }

        private void SaveFileButton_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new SaveFileDialog();
            if (dialog.ShowDialog() == true)
            {
                var model = GetModel();
                model.Contracts = null;
                FileStorage.Save(model, dialog.FileName);
            }
        }
    }
}
