﻿using Lab9.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;

namespace Lab9
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private IStorage Storage { get; set; }
        private Queries Queries { get; set; }

        private Border tourSavedRow;
        private Border contractSavedRow;
        private Border query2SavedRow;

        private MemoryStorage savedMemoryStorage;

        private Func<Tour, object> toursSort = t => t.Id;
        private Func<Contract, object> contractSort = c => c.Id;

        private Tour tourFilter = new Tour() { Begin = DateTime.MinValue, Days = -1 };
        private Contract contractFilter = new Contract() { Cost = -1, DateTime = DateTime.MinValue, Places = -1, Tour = new Tour() };

        public MainWindow()
        {
            InitializeComponent();

            Storage = new MemoryStorage();
            Queries = new Queries { Storage = Storage };
        }

        private void SwitchSourceMemory()
        {
            if (Storage is MemoryStorage)
                return;

            Storage = savedMemoryStorage ?? new MemoryStorage();
            Queries = new Queries { Storage = Storage };

            ResetTours();
            ResetContracts();
            ResetQueries();
        }

        private void SwitchSourceDB()
        {
            if (Storage is AppDbContext)
                return;

            savedMemoryStorage = (MemoryStorage)Storage;
            var dbContext = new AppDbContext();
            dbContext.Database.CreateIfNotExists();

            Storage = dbContext;
            Queries = new Queries { Storage = Storage };

            ResetTours();
            ResetContracts();
            ResetQueries();
        }

        private void ResetTours()
        {
            Clear(toursTable);
            var tours = Storage.Tours;
            tours = tours.OrderBy(toursSort);
            tours = ApplyToursFilter(tours);

            foreach (var tour in tours)
            {
                var row = tourSavedRow.DeepCopy();
                SetSingleTour(row, tour);
                toursTable.Children.Add(row);
            }
        }

        private void ResetContracts()
        {
            Clear(contractsTable);
            var contracts = Storage.Contracts;
            contracts = contracts.OrderBy(contractSort);
            contracts = ApplyContractFilters(contracts);

            foreach (var contract in contracts)
            {
                var row = contractSavedRow.DeepCopy();
                SetSingleContract(row, contract);
                contractsTable.Children.Add(row);
            }
        }

        private IEnumerable<Tour> ApplyToursFilter(IEnumerable<Tour> tours)
        {
            if (!string.IsNullOrWhiteSpace(tourFilter.Title))
                tours = tours.Where(t => t.Title.ToLower().Contains(tourFilter.Title.ToLower()));
            if (tourFilter.Days != -1)
                tours = tours.Where(t => t.Days >= tourFilter.Days - 5 && t.Days <= tourFilter.Days + 5);
            if (!string.IsNullOrWhiteSpace(tourFilter.RestPlace))
                tours = tours.Where(t => t.RestPlace.ToLower().Contains(tourFilter.RestPlace.ToLower()));
            if (tourFilter.Begin != DateTime.MinValue)
                tours = tours.Where(t => t.Begin <= tourFilter.Begin && t.End >= tourFilter.Begin);
            if (!string.IsNullOrWhiteSpace(tourFilter.HotelLevel))
                tours = tours.Where(t => t.HotelLevel.ToLower().Contains(tourFilter.HotelLevel.ToLower()));
            return tours;
        }

        private IEnumerable<Contract> ApplyContractFilters(IEnumerable<Contract> contracts)
        {
            if (!string.IsNullOrWhiteSpace(contractFilter.Number))
                contracts = contracts.Where(c => c.Number.ToLower().Contains(contractFilter.Number.ToLower()));
            if (!string.IsNullOrWhiteSpace(contractFilter.Tour.Title))
                contracts = contracts.Where(c => c.Tour.Title.ToLower().Contains(contractFilter.Tour.Title.ToLower()));
            if (contractFilter.DateTime != DateTime.MinValue)
                contracts = contracts.Where(c => c.DateTime == contractFilter.DateTime);
            if (!string.IsNullOrWhiteSpace(contractFilter.ClientName))
                contracts = contracts.Where(c => c.ClientName.ToLower().Contains(contractFilter.ClientName.ToLower()));
            if (!string.IsNullOrWhiteSpace(contractFilter.EmployeeName))
                contracts = contracts.Where(c => c.ClientName.ToLower().Contains(contractFilter.EmployeeName.ToLower()));
            if (!string.IsNullOrWhiteSpace(contractFilter.ClientPhone))
                contracts = contracts.Where(c => c.ClientName.ToLower().Contains(contractFilter.ClientPhone.ToLower()));
            if (contractFilter.Cost != -1)
                contracts = contracts.Where(c => c.Cost >= contractFilter.Cost - 100 && c.Cost <= contractFilter.Cost + 100);
            if (contractFilter.Places != -1)
                contracts = contracts.Where(c => c.Places >= contractFilter.Places - 1 && c.Places <= contractFilter.Places + 1);
            return contracts;
        }

        private void ResetQueries()
        {
            ResetQuery1();
            ResetQuery2();
            ResetQuery3();
            ResetQuery4();
        }

        private void ResetQuery1()
        {
            if (query1DateTime.SelectedDate.HasValue)
            {
                Clear(query1Table);
                var contracts = Queries.ContractsByDate(query1DateTime.SelectedDate.Value);

                foreach (var contract in contracts.OrderBy(contractSort))
                {
                    var row = contractSavedRow.DeepCopy();
                    SetSingleContract(row, contract);
                    query1Table.Children.Add(row);
                }
            }
        }

        private void ResetQuery2()
        {
            Clear(query2Table);
            var employees = Queries.EmployeesContracts();

            foreach (var employee in employees)
            {
                var row = query2SavedRow.DeepCopy();
                var grid = (DockPanel)row.Child;
                ((Label)grid.Children[0]).Content = employee.Employee;
                ((Label)grid.Children[1]).Content = employee.Count;
                query2Table.Children.Add(row);
            }
        }

        private void ResetQuery3()
        {
            if (int.TryParse(query3Text.Text, out var duration))
            {
                query3Text.Background = Brushes.White;

                Clear(query3Table);
                var tours = Queries.ToursMinDuration(duration);

                foreach (var tour in tours.OrderBy(toursSort))
                {
                    var row = tourSavedRow.DeepCopy();
                    SetSingleTour(row, tour);
                    query3Table.Children.Add(row);
                }
            }
            else
            {
                if (string.IsNullOrWhiteSpace(query3Text.Text))
                    query3Text.Background = Brushes.White;
                else
                    query3Text.Background = new SolidColorBrush(Color.FromArgb(255, 255, 77, 77));
            }
        }

        private void ResetQuery4()
        {
            if (!string.IsNullOrWhiteSpace(query4Text.Text))
            {
                Clear(query4Table);
                var tours = Queries.ToursByClient(query4Text.Text);

                foreach (var tour in tours.OrderBy(toursSort))
                {
                    var row = tourSavedRow.DeepCopy();
                    SetSingleTour(row, tour);
                    query4Table.Children.Add(row);
                }
            }
        }

        private void SetSingleTour(Border row, Tour tour)
        {
            var dockPanel = (DockPanel)row.Child;
            SetPanel(dockPanel, 3, tour.Title);
            SetPanel(dockPanel, 4, tour.Days + " дней");
            SetPanel(dockPanel, 5, tour.RestPlace);
            SetPanel(dockPanel, 6, tour.Begin.ToString("dd.MM.yyyy") + " - " + tour.End.ToString("dd.MM.yyyy"));
            SetPanel(dockPanel, 7, tour.HotelLevel);
            ((Label)dockPanel.Children[0]).MouseDown += (s, e) => TourEditClick(tour);
            ((Label)dockPanel.Children[1]).MouseDown += (s, e) => TourDeleteClick(tour);
        }

        private void TourEditClick(Tour tour) => new EditTourWindow(Storage, () => ResetTours(), tour.Id).Show();

        private void TourDeleteClick(Tour tour)
        {
            if (MessageBox.Show("Вы уверены, что хотите удалить " + tour.Title, "Удаление тура", MessageBoxButton.OKCancel, MessageBoxImage.Question) == MessageBoxResult.OK)
            {
                Storage.Remove(tour);
                Storage.SaveChanges();
                ResetTours();
            }
        }

        private void SetSingleContract(Border row, Contract contract)
        {
            var dockPanel = (DockPanel)row.Child;
            SetPanel(dockPanel, 3, contract.Number);
            SetPanel(dockPanel, 4, contract.Tour.Title);
            SetPanel(dockPanel, 5, contract.DateTime.ToString("dd.MM.yyyy"));
            SetPanel(dockPanel, 6, contract.EmployeeName);
            SetPanel(dockPanel, 7, contract.ClientName);
            SetPanel(dockPanel, 8, contract.ClientPhone);
            SetPanel(dockPanel, 9, contract.Cost.ToString("n") + " Br");
            SetPanel(dockPanel, 10, contract.Places + " мест");
            ((Label)dockPanel.Children[0]).MouseDown += (s, e) => ContractEditClick(contract);
            ((Label)dockPanel.Children[1]).MouseDown += (s, e) => ContractDeleteClick(contract);
        }

        private void ContractEditClick(Contract contract) => new EditContractWindow(Storage, () => ResetContracts(), contract.Id).Show();

        private void ContractDeleteClick(Contract contract)
        {
            if (MessageBox.Show("Вы уверены, что хотите удалить " + contract.Number, "Удаление договора", MessageBoxButton.OKCancel, MessageBoxImage.Question) == MessageBoxResult.OK)
            {
                Storage.Remove(contract);
                Storage.SaveChanges();
                ResetContracts();
            }
        }

        private void SetPanel(DockPanel panel, int index, string text)
        {
            var label = (Label)panel.Children[index];
            label.Content = text;
        }

        private void Clear(DockPanel table)
        {
            if (table == toursTable || table == contractsTable)
            {
                var header = table.Children[1];
                var filter = table.Children[0];
                table.Children.Clear();
                table.Children.Add(filter);
                table.Children.Add(header);
            }
            else
            {
                var header = table.Children[0];
                table.Children.Clear();
                table.Children.Add(header);
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            tourSavedRow = tourRow.DeepCopy();
            contractSavedRow = contractRow.DeepCopy();
            query2SavedRow = query2Row.DeepCopy();

            Clear(toursTable);
            Clear(contractsTable);

            Clear(query1Table);
            Clear(query2Table);
            Clear(query3Table);
            Clear(query4Table);

            ResetTours();
            ResetContracts();
        }

        private void SetTourSort(Func<Tour, object> predicate)
        {
            toursSort = predicate;
            ResetTours();
        }

        private void SetContractSort(Func<Contract, object> predicate)
        {
            contractSort = predicate;
            ResetContracts();
        }

        private void Window_Closed(object sender, EventArgs e) => Storage.Dispose();

        private void TabItemTours_GotFocus(object sender, RoutedEventArgs e) => ResetTours();
        private void TabItemContracts_GotFocus(object sender, RoutedEventArgs e) => ResetContracts();
        private void TabItemQueries_GotFocus(object sender, RoutedEventArgs e) => ResetQueries();

        private void Query1DateTime_SelectedDateChanged(object sender, SelectionChangedEventArgs e) => ResetQuery1();
        private void Query2Expander_Expanded(object sender, RoutedEventArgs e) => ResetQuery2();
        private void Query3Text_TextChanged(object sender, TextChangedEventArgs e) => ResetQuery3();
        private void Query4Text_TextChanged(object sender, TextChangedEventArgs e) => ResetQuery4();
        private void SourceMemoryMenuItem_Click(object sender, RoutedEventArgs e) => SwitchSourceMemory();
        private void SourceDBMenuItem_Click(object sender, RoutedEventArgs e) => SwitchSourceDB();

        private void TourTitleLabel_MouseDown(object sender, MouseButtonEventArgs e) => SetTourSort(t => t.Title);
        private void TourDaysLabel_MouseDown(object sender, MouseButtonEventArgs e) => SetTourSort(t => t.Days);
        private void TourPlaceLabel_MouseDown(object sender, MouseButtonEventArgs e) => SetTourSort(t => t.RestPlace);
        private void TourDatesLabel_MouseDown(object sender, MouseButtonEventArgs e) => SetTourSort(t => t.Begin);
        private void TourLevelLabel_MouseDown(object sender, MouseButtonEventArgs e) => SetTourSort(t => t.HotelLevel);

        private void ContractNumberLabel_MouseDown(object sender, MouseButtonEventArgs e) => SetContractSort(c => c.Number);
        private void ContractTourLabel_MouseDown(object sender, MouseButtonEventArgs e) => SetContractSort(c => c.Tour.Title);
        private void ContractDateLabel_MouseDown(object sender, MouseButtonEventArgs e) => SetContractSort(c => c.DateTime);
        private void ContractEmployeeLabel_MouseDown(object sender, MouseButtonEventArgs e) => SetContractSort(c => c.EmployeeName);
        private void ContractClientLabel_MouseDown(object sender, MouseButtonEventArgs e) => SetContractSort(c => c.ClientName);
        private void ContractPhoneLabel_MouseDown(object sender, MouseButtonEventArgs e) => SetContractSort(c => c.ClientPhone);
        private void ContractCostLabel_MouseDown(object sender, MouseButtonEventArgs e) => SetContractSort(c => c.Cost);
        private void ContractPlacesLabel_MouseDown(object sender, MouseButtonEventArgs e) => SetContractSort(c => c.Places);

        #region Tour filters
        private void TourTitleFilter_TextChanged(object sender, TextChangedEventArgs e)
        {
            tourFilter.Title = tourFilterTitle.Text;
            ResetTours();
        }
        private void TourDaysFilter_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (int.TryParse(tourFilterDays.Text, out var days))
            {
                tourFilter.Days = days;
                ResetTours();
            }
            else
                days = -1;
        }
        private void TourPlaceFilter_TextChanged(object sender, TextChangedEventArgs e)
        {
            tourFilter.RestPlace = tourFilterPlace.Text;
            ResetTours();
        }
        private void TourDatesFilter_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (DateTime.TryParse(tourFilterDates.Text, out var dates))
            {
                tourFilter.Begin = dates;
                ResetTours();
            }
            else
                tourFilter.Begin = DateTime.MinValue;
        }
        private void TourLevelFilter_TextChanged(object sender, TextChangedEventArgs e)
        {
            tourFilter.HotelLevel = tourFilterLevel.Text;
            ResetTours();
        }
        #endregion

        #region ContractFilters
        private void ContractNumberFilter_MouseDown(object sender, TextChangedEventArgs e)
        {
            contractFilter.Number = contractFilterNumber.Text;
            ResetContracts();
        }
        private void ContractTourFilter_MouseDown(object sender, TextChangedEventArgs e)
        {
            contractFilter.Tour.Title = contractFilterTour.Text;
            ResetContracts();
        }
        private void ContractDateFilter_MouseDown(object sender, TextChangedEventArgs e)
        {
            if (DateTime.TryParse(contractFilterDate.Text, out var date))
            {
                contractFilter.DateTime = date;
                ResetContracts();
            }
            else
                contractFilter.DateTime = DateTime.MinValue;
        }
        private void ContractEmployeeFilter_MouseDown(object sender, TextChangedEventArgs e)
        {
            contractFilter.EmployeeName = contractFilterEmployee.Text;
            ResetContracts();
        }
        private void ContractClientFilter_MouseDown(object sender, TextChangedEventArgs e)
        {
            contractFilter.ClientName = contractFilterClient.Text;
            ResetContracts();
        }
        private void ContractPhoneFilter_MouseDown(object sender, TextChangedEventArgs e)
        {
            contractFilter.ClientPhone = contractFilterPhone.Text;
            ResetContracts();
        }
        private void ContractCostFilter_MouseDown(object sender, TextChangedEventArgs e)
        {
            if (decimal.TryParse(contractFilterCost.Text, out var cost))
            {
                contractFilter.Cost = cost;
                ResetContracts();
            }
            else
                contractFilter.Cost = -1;
        }
        private void ContractPlacesFilter_MouseDown(object sender, TextChangedEventArgs e)
        {
            if (int.TryParse(contractFilterPlaces.Text, out var places))
            {
                contractFilter.Places = places;
                ResetContracts();
            }
            else
                contractFilter.Places = -1;
        }
        #endregion

        private void AddTourMenuItem_Click(object sender, RoutedEventArgs e) => new EditTourWindow(Storage, () => ResetTours()).Show();

        private void AddContractMenuItem_Click(object sender, RoutedEventArgs e) => new EditContractWindow(Storage, () => ResetContracts()).Show();
    }

    public static class IUElementExtensions
    {
        public static TElement DeepCopy<TElement>(this TElement element) where TElement: UIElement
        {
            var xaml = XamlWriter.Save(element);
            var xamlString = new StringReader(xaml);
            var xmlTextReader = new XmlTextReader(xamlString);
            var deepCopyObject = (TElement)XamlReader.Load(xmlTextReader);
            return deepCopyObject;
        }
    }
}
