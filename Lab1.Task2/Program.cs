﻿using System;

namespace Lab1.Task2
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var a = 0.0;
            var b = 0.0;
            var h = 0.0;
            var eps = 0.0;
            try
            {
                a = Input("a");
                b = Input("b");
                h = Input("h", true);
                eps = Input("eps", true);
            }
            catch (FormatException e)
            {
                Console.WriteLine(e.Message);
                return;
            }

            Console.WriteLine("   x \t\t Calculated \t Count \t\t Actual");
            for (var x = a; x <= b; x += h)
            {
                var calculated = Calculate(x, eps, out var count);
                var actual = CalculateActual(x);

                Console.WriteLine($"   {x} \t\t{calculated:F6} \t{count} \t\t{actual:F6}");
            }
            Console.Read();
        }

        private static double Calculate(double x, double eps, out int count)
        {
            var sum = 0.0;
            for (var i = 0; i < 10000000; i++)
            {
                var delta = (i % 2 != 0 ? -1.0 : 1.0) * Math.Pow(x, 2 * i) / Factorial(i);
                sum += delta;
                if (Math.Abs(delta) < eps)
                {
                    count = i + 1;
                    return sum;
                }
            }
            throw new OverflowException("Too much iterations");
        }

        private static double Factorial(int x)
        {
            var result = 1.0;
            for (var i = 1; i <= x; i++)
                result *= i;
            return result;
        }

        private static double CalculateActual(double x) => Math.Exp(-(x * x));

        private static float Input(string name, bool positive = false)
        {
            Console.Write($"Enter {name}: ");
            var input = Console.ReadLine();
            var val = 0f;
            if (!float.TryParse(input, out val))
                throw new FormatException($"{name} must be a float number");
            if (positive && val <= 0)
                throw new FormatException($"{name} must be a positive number");
            return val;
        }
    }
}
