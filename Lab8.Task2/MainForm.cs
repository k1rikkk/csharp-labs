﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab8.Task2
{
    public partial class MainForm : Form
    {
        public int[] list = new int[] { };

        public MainForm()
        {
            InitializeComponent();
        }

        private void AboutToolStripMenuItem_Click(object sender, EventArgs e) => MessageBox.Show("Ковалёв К.А. ПО-13 Лабораторная работа 8");

        private void ExitToolStripMenuItem_Click(object sender, EventArgs e) => Close();

        private void FileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var dialog = new OpenFileDialog();
            if (dialog.ShowDialog() != DialogResult.OK)
                return;

            list = JsonConvert.DeserializeObject<int[]>(File.ReadAllText(dialog.FileName));
            animateToolStripMenuItem.Enabled = true;
            Redraw();
        }

        private async void AnimateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            for (var i = 0; i < list.Length - 1; i++)
            {
                Redraw(i);

                var min = i;
                for (var j = i + 1; j < list.Length; j++)
                    if (list[min] > list[j])
                        min = j;

                var temp = list[min];
                list[min] = list[i];
                list[i] = temp;

                await Task.Delay(1000);
            }

            Redraw();

            MessageBox.Show(string.Join(", ", list));
        }

        private void Redraw(int? cur = null)
        {
            var points = chart.Series.First().Points;
            points.Clear();
            chart.Series.Last().Points.Clear();

            for (var i = 0; i < list.Length; i++)
            {
                if (i == cur)
                    continue;
                points.AddXY(i, list[i]);
            }

            if (cur.HasValue)
                chart.Series.Last().Points.AddXY(cur, list[cur.Value]);
        }
    }
}
