﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2.Task2
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Console.WriteLine("Enter float elements of array, separate by commas:");
            var list = new float[] { };
            try
            {
                list = Console.ReadLine().Split(new[] { " , ", ", ", " ,", "," }, StringSplitOptions.RemoveEmptyEntries).Select(x => float.Parse(x)).ToArray();
            }
            catch (FormatException)
            {
                Console.WriteLine("Can\'t parse array. Enter float elements of array, separate by commas: ");
                return;
            }

            var index = 0;
            for (index = list.Length - 1; index >= 0; index--)
                if (list[index] > 0)
                    break;

            list = list.Take(index + 1).OrderBy(x => x).Concat(list.Skip(index + 1)).ToArray();
            Console.WriteLine("Result:");
            Console.WriteLine(string.Join(", ", list));
            Console.Read();
        }
    }
}
