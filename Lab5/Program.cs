﻿using System;
using System.IO;
using System.Text;

namespace Lab5
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var replace = "eyuioa";
            var fileName = "text.txt";
            var text = "";
            try
            {
                text = File.ReadAllText(fileName);
            }
            catch (FileNotFoundException)
            {
                Console.WriteLine("File not found");
                Console.Read();
                return;
            }

            Console.WriteLine("Initial:\n\r" + text);

            text = " " + text;
            var result = new StringBuilder(text.Length - 1);
            for (int i = 1; i < text.Length; i++)
                if (char.IsLetter(text[i]) && !char.IsLetter(text[i - 1]) && replace.IndexOf(text[i]) >= 0)
                    result.Append(char.ToUpper(text[i]));
                else
                    result.Append(text[i]);
            Console.WriteLine("Result:\n\r" + result.ToString());
            Console.Read();
        }
    }
}
