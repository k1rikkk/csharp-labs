﻿using System;
using System.Linq;

namespace Lab3.Task1
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Console.Write("Enter task number: ");
            var t = -1;
            if (!int.TryParse(Console.ReadLine(), out t))
            {
                Console.WriteLine("Task number must be an integer");
                Console.Read();
                return;
            }
            try
            {
                switch (t)
                {
                    case 1:
                        Task1();
                        break;
                    case 2:
                        Task2();
                        break;
                    case 3:
                        Task3();
                        break;
                    case 4:
                        Task4();
                        break;
                }
            }
            catch (FormatException e)
            {
                Console.WriteLine(e.Message);
            }
            Console.Read();
        }

        private struct Cartoon
        {
            public string Title { get; set; }
            public TimeSpan Duration { get; set; }
            public string Country { get; set; }
            public decimal[] Prices { get; }

            public Cartoon(string title, TimeSpan duration, string country, params decimal[] prices)
            {
                if (prices.Length != 3)
                    throw new FormatException("Cartoon must have 3 prices");

                Title = title;
                Duration = duration;
                Country = country;
                Prices = prices;
            }
        }

        private static void Task4()
        {
            Cartoon[] cartoons = new Cartoon[5]
            {
                new Cartoon("Cartoon 1", TimeSpan.Parse("00:45:00"), "Country 1", 13, 14, 15),
                new Cartoon("Cartoon 2", TimeSpan.Parse("00:30:00"), "Country 1", 10, 11, 14),
                new Cartoon("Cartoon 3", TimeSpan.Parse("01:45:00"), "Country 2", 13, 12, 9),
                new Cartoon("Cartoon 4", TimeSpan.Parse("00:20:00"), "Country 2", 12, 12, 13),
                new Cartoon("Cartoon 5", TimeSpan.Parse("00:15:00"), "Country 3", 6, 17, 9),
            };
            var input = ("Cartoon 3", 2018, TimeSpan.Parse("00:30:00"), 13);
            var output = Task4Process(input, cartoons);
            foreach (var single in output)
                Console.WriteLine(single);
        }

        private static (string, string, string, decimal, bool)[] Task4Process((string, int, TimeSpan, decimal) input, Cartoon[] list) => list.Select(x => (x.Title, x.Country, x.Country + " " + input.Item2, (decimal)Math.Round(x.Prices.Average(), 3), input.Item3 == x.Duration)).ToArray();

        private static void InputArray(out float[] list)
        {
            Console.WriteLine("Enter float elements of array, separate by commas:");
            try
            {
                list = Console.ReadLine().Split(new[] { " , ", ", ", " ,", "," }, StringSplitOptions.RemoveEmptyEntries).Select(x => float.Parse(x)).ToArray();
            }
            catch (FormatException e)
            {
                throw new FormatException("Can\'t parse array. Enter float elements of array, separate by commas: ", e);
            }
        }

        private static void Task1()
        {
            InputArray(out var list);
            var mul = 1f;
            foreach (var x in list)
                if (x < 0)
                    mul *= x;
            Console.WriteLine($"Multiplication of negative elements: {mul}");
        }

        private static void Task2()
        {
            InputArray(out var list);

            var index = 0;
            for (index = list.Length - 1; index >= 0; index--)
                if (list[index] > 0)
                    break;

            list = list.Take(index + 1).OrderBy(x => x).Concat(list.Skip(index + 1)).ToArray();
            Console.WriteLine("Result:");
            Console.WriteLine(string.Join(", ", list));
        }

        private static void InputRowsCols(ref int rows, ref int cols)
        {
            rows = 0;
            Console.Write("Enter number of rows: ");
            if (!int.TryParse(Console.ReadLine(), out rows))
                throw new FormatException("Number of rows must be an integer number");
            Console.Write("Enter number of cols: ");
            cols = 0;
            if (!int.TryParse(Console.ReadLine(), out cols))
                throw new FormatException("Number of columns must be an integer number");
        }

        private static void Task3()
        {
            var rows = 0;
            var cols = 0;
            InputRowsCols(ref rows, ref cols);
            var array = new float[rows, cols];
            var ans = -1;
            for (var j = 0; j < cols; j++)
            {
                bool hasZero = false;
                for (var i = 0; i < rows; i++)
                {
                    Console.Write($"a[{i}, {j}] = ");
                    if (!float.TryParse(Console.ReadLine(), out array[i, j]))
                    {
                        Console.WriteLine("Elements must be a float number");
                        return;
                    }
                    if (array[i, j] == 0)
                        hasZero = true;
                }
                if (hasZero)
                    ans = j;
            }
            if (ans == -1)
                Console.WriteLine("Array doesn\'t contains zeros");
            else
                Console.WriteLine($"Last column containing zeros is {ans + 1}");
        }
    }
}
